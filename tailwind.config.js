/** @type {import('tailwindcss').Config} */
module.exports = {
  prefix: '',
  content: ['./src/**/*.{html, ts}' /* ... */],
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
