import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  banner: any[] = [
    {
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      title: "Women's latest fashion sale",
      sub_title: 'Trending item',
      text: '',
    },
    {
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      title: "Women's latest fashion sale",
      sub_title: 'Trending item',
      text: '',
    },
    {
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      title: "Women's latest fashion sale",
      sub_title: 'Trending item',
      text: '',
    },
  ];

  categories: any[] = [
    {
      img: 'http://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
    {
      img: 'hhttp://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
    {
      img: 'hhttp://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
    {
      img: 'hhttp://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
    {
      img: 'hhttp://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
    {
      img: 'hhttp://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
    {
      img: 'hhttp://via.placeholder.com/150x150',
      title: 'Dress & frock',
      amt: '56',
    },
  ];

  accordion: any[] = [
    {
      title: 'Dress & frock',
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      data: [
        {
          name: 'Shirt',
          title: 'Avaliable Colors',
          price: '100'
        }
      ]
    }
  ];

  showcase_container: any[] = [
    {
      theme: 'new arrival',
      data: [
        {
          img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
          title: 'Dress & frock',
          category: 'Clothes',
          current_price: '$100',
          normal_price: '$1100',
        },
        {
          img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
          title: 'Dress & frock',
          category: 'Clothes',
          current_price: '$100',
          normal_price: '$1100',
        },

      ],
    },
    {
      theme: 'new arrival',
      data: [
        {
          img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
          title: 'Dress & frock',
          category: 'Clothes',
          current_price: '$100',
          normal_price: '$1100',
        },
        {
          img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
          title: 'Dress & frock',
          category: 'Clothes',
          current_price: '$100',
          normal_price: '$1100',
        },

      ],
    },

  ];

  deals_of_day: any[] = [
    {
      title: 'Deals of the Day',
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      rating: [],
      prod_title: 'shampoo, conditioner & facewash packs',
      desc: 'lorem Ipsum, Lorem Ipsum, ullipsum, ullr&auml;um, ullr&auml;um, ullr&auml;um, ullr&aum',
      price: '$1100',
      total: 40,
      sold: 20,
    },
    {
      title: 'Deals of the Day',
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      rating: [],
      prod_title: 'shampoo, conditioner & facewash packs',
      desc: 'lorem Ipsum, Lorem Ipsum, ullipsum, ullr&auml;um, ullr&auml;um, ullr&auml;um, ullr&aum',
      price: '$1100',
      total: 40,
      sold: 20,
    }
  ];

  product_card: any[] = [
    {
      img:['https://preview.cruip.com/mosaic/images/applications-image-21.jpg' , 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg' ],
      category: 'Clothes',
      title: 'Mens Winter Leathers Jackets',
      price: '$1100',
    },
    {
      img:['https://preview.cruip.com/mosaic/images/applications-image-21.jpg' , 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg' ],
      category: 'Clothes',
      title: 'Mens Winter Leathers Jackets',
      price: '$1100',
    },
    {
      img:['https://preview.cruip.com/mosaic/images/applications-image-21.jpg' , 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg' ],
      category: 'Clothes',
      title: 'Mens Winter Leathers Jackets',
      price: '$1100',
    },
  ];


  constructor() { }

  ngOnInit(): void {
  }

}
