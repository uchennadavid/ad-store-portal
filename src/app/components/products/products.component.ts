import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {


  accordion: any[] = [
    {
      title: 'Dress & frock',
      img: 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg',
      data: [
        {
          name: 'Shirt',
          title: 'Avaliable Colors',
          price: '100'
        }
      ]
    }
  ];



  product_card: any[] = [
    {
      img:['https://preview.cruip.com/mosaic/images/applications-image-21.jpg' , 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg' ],
      category: 'Clothes',
      title: 'Mens Winter Leathers Jackets',
      price: '$1100',
    },
    {
      img:['https://preview.cruip.com/mosaic/images/applications-image-21.jpg' , 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg' ],
      category: 'Clothes',
      title: 'Mens Winter Leathers Jackets',
      price: '$1100',
    },
    {
      img:['https://preview.cruip.com/mosaic/images/applications-image-21.jpg' , 'https://preview.cruip.com/mosaic/images/applications-image-21.jpg' ],
      category: 'Clothes',
      title: 'Mens Winter Leathers Jackets',
      price: '$1100',
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
